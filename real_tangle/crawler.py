import requests
from iota import TryteString


id = 0

url_base = "http://chronicle-0.chrysalis-mainnet.iotaledger.net/api/mainnet/messages/"
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

def get_childs(id):
    url = url_base + id + "/children"

    result = requests.get(url, headers=headers)
    hashes = result.json()["data"]["childrenMessageIds"]

    return hashes

def get_timestamp(hash):
    url = url_base + hash
    result = requests.get(url, headers=headers)


    if result.json()["data"]["payload"]["type"] != 0:
        return None
    else:
        return result.json()["data"]["payload"]["timestamp"]

def get_type(hash):
    url = url_base + hash
    result = requests.get(url, headers=headers)

    try:
        return result.json()["data"]["payload"]["type"]
    except:
        print(result.content)


def iterate_tangle(hash):
    global to_iterate
    childs = get_childs(hash)
    output_string = ""
    for child in childs:
        if child not in crawled:
            ids[child] = new_id()
            crawled.add(child)

            type_child = get_type(child)

            if type_child == 1:
                ids[child] = "MMMMMMMMM" + str(ids[child])

            to_iterate = to_iterate + [[0, child]]
            output_string += str(ids[hash]) + " -> " + str(ids[child]) + "\n"
    f = open(output_file, "a")
    f.write(output_string)
    f.close()


def new_id():
    global id
    id +=1
    return id

start_id = "69356c397b161cc055a3889ab6ac489724ff6ea1d799490e92dd9a79d09baeb1"

to_crawl = set()
crawled = set()

to_iterate = [[get_timestamp(start_id), start_id]]
nb_iterated = 1

output_file = "tangle.txt"
f = open(output_file, "w")
f.write("")
f.close()

ids = {start_id : id}
while len(crawled)<20000:
    if len(to_iterate) == 0:
        print("No more childs")
        exit(0)

    hash = to_iterate[0][1]
    del to_iterate[0]

    print(len(crawled))
    nb_iterated += 1
    iterate_tangle(hash)

print(crawled)
