"""

Lib files for drawing graphs
"""

import networkx as nx
import matplotlib.pylab as plt
import os

def draw_figure(dico, fig=None, ax=None, file=None):
    """
    Draw networkx 
    
    :param dico: {ID: [Childs keys]}
    :param file: Where to save. If None, no saving.
    :param ax: axis of a matplotlib figure
    :param fig: figure matplotlib
    :rparam: (fig, ax) 
    """
    
    G = nx.DiGraph()
    
    for x, lst in dico.items():
        for y in lst:
            G.add_edge(x, y)
            
    if fig is None:
        fig, ax = plt.subplots(figsize=(15, 15))
        
    nx.draw(G, ax=ax)
    plt.tight_layout()
    
    if file is not None:
        fig.savefig(file)
        
    return fig, ax
        

def draw_networkx(dico, file="test.png"):
    """Embedd with networkx
    
    :param dico: {parent: [childs]}
    :rparam: None
    """
    G = nx.DiGraph()
    for x, vals in dico.items():
        for y in vals:
            G.add_edge(x, y)
    
    A = nx.drawing.nx_agraph.to_agraph(G)
    A.layout('dot')
    A.draw(file)
    
    return

def set_xlimylim(ax):
    """
    Allows to get x and y axis with same scaling
    
    :param ax: figure axis
    :rparam: None
    """
    a0, a1 = ax.set_xlim()
    b0, b1 = ax.set_ylim()
    
    d0 = a1 - a0
    d1 = b1 - b0
    
    if d0 > d1:
        dd = (d0 - d1)/2
        ax.set_ylim(b0-dd, b1+dd)
    else:
        dd = (d1 - d0)/2
        ax.set_xlim(a0-dd, a1+dd)