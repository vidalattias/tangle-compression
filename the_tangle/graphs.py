"""

Functions to work with graphs.

Often consider the unweighted directed graph

{ 
    x0: [x1, x2],
    x1:[x2],
    x2:[]
}
"""
import numpy as np

def reverse_dict(dic_childs, safe=False):
    """Build the parent dictionary, 
    
    ie reverse the DAG
    ie, key are childs, list are parents.
    
    Assume that keys are complete (ie all children have at least an empty key {c:[]}, else fail)
    
    :param dic_childs: {key: [childs]}
    :param safe: assume that the dict is complete
    :rparam: {key: [parents]}
    """

    dic_parents = dict(list(map(lambda x: (x, []), dic_childs.keys())))

    if safe:
        for k, val in dic_childs.items():
            for ki in val:
                if ki in dic_parents:
                    dic_parents[ki].append(k)


    else:
        for k, val in dic_childs.items():
            for ki in val:
                dic_parents[ki].append(k)

    return dic_parents



def get_top_nodes(S, dic_parent):
    """On a set of nodes, find the ones that are at the boundaries of the group, 
    and from which all are accessible
    
    :param S: set/list of nodes to look at
    :param dic_parent: relationship {child: [parents]}
    :rparam: Set of valid nodes
    """
    valid = set()

    for s in S:
        b = True
        for si in dic_parent[s]:
            # not a top node
            if si in S:
                b = False
                break
        if b:
            valid.add(s)

    return valid



def depth_min(gpx, dic_c, dic_p):
    """

    Compute the depth of each nodes in the groups, starting from node without parent in the group
    :param gpx: set of nodes
    :param dic_c: {key: [childs]}
    :param dic_p: {key: [parents]}
    :rparam: {key: depth}
    """

    tops = get_top_nodes(gpx, dic_p)

    dic_depth = dict([(c, len(gpx)) for c in gpx])
    for c in tops:
        dic_depth[c] = 0

    # Counter that get tracks of how many parents have a node in the group
    dic_cnt = dict([(c, len(gpx & set(dic_p[c]))) for c in gpx])

    S_run = tops
    while len(S_run) != 0:
        k = S_run.pop()
        d = dic_depth[k]+1

        for c in set(dic_c[k]) & gpx:
            dic_depth[c] = min(dic_depth[c], d)
            dic_cnt[c] -= 1
            if dic_cnt[c] == 0:
                S_run.add(c)

    return dic_depth

def depth_max(gpx, dic_c, dic_p, verbose=False):
    """
    Compute the depth of each nodes in the groups, starting from node without parent in the group
    Maximal path to reach a node
    
    :param gpx: set of nodes
    :param dic_c: {key: [childs]}
    :param dic_p: {key: [parents]}
    :rparam: {key: depth}
    """

    tops = get_top_nodes(gpx, dic_p)

    dic_depth = dict([(c, 0) for c in gpx])

    # Counter that get tracks of how many parents have a node in the group
    dic_cnt = dict([(c, len(gpx & set(dic_p[c]))) for c in gpx])

    S_run = tops
    while len(S_run) != 0:
        k = S_run.pop()
        d = dic_depth[k] + 1

        for c in set(dic_c[k]) & gpx:
            dic_depth[c] = max(dic_depth[c], d)
            dic_cnt[c] -= 1
            if dic_cnt[c] == 0:
                S_run.add(c)

    if verbose:
        l0 = sum(list(dic_cnt.values()))
        if l0 != 0:
            print("No zero count")

    return dic_depth


def jaccard_similarity_reverse(dic0, verbose=False):
    """Jaccard similarity fastened by reverse search
    
    {c: [bi, wi]} ==> {bi: [c, wi]}
    
    :param dic0: dictionnary 
    :rparam: matrix of size l x l, following dic order
    """
    dic_idx = dict([(c, idx) for idx, c in enumerate(dic0)])
    l = len(dic0)

    dic_interm = {}
    for k, vals in dic0.items():
        for vi, wi in vals.items():
            if vi not in dic_interm:
                dic_interm[vi] = []
            dic_interm[vi].append([dic_idx[k], wi])

    # Jaccard computation
    n = len(dic_interm)
    mat0 = np.zeros((l, l))
    for idx, vi in enumerate(dic_interm):

        if verbose & (idx % 10000 == 0):
            print("{} \t/ {}".format(idx, n), end="\r")

        msk, wei = np.array(dic_interm[vi]).T
        msk = msk.astype(int)
        for i in msk:
            mat0[i, msk] += wei

    diag = mat0.diagonal()
    return (mat0 + mat0.T) / (diag[:, np.newaxis] + diag)



def affinity_PageRank(pt, dic, alpha=1, beta=0.5, kmax=4, tol=0.00001):
    """
    Affinity based on average linkage
    f(nei(i), nei(j))
    
    :param dic: direct dictionary
    
    :param alpha: Decay factor 
    :param beta:  Balance for output nodes
    :param kmax: maximal distance to go to
    :param tol: limit value to iterate
    
    :rparam: dictionary of distance
    """
    dic_d = {pt: 1} # Self contribution of 1
    S_see = dict([(pt, 1)]) # Point and contrib, as we may pass several times over a node

    # Loop for kmax times, so max min dist at kmax
    for _ in range(1, kmax+1):
        S_tosee = dict() # Reset
        for ci, v in S_see.items():
            if v < tol:
                continue

            childs = dic[ci]
            l_out  = len(childs)

            # Iter over all connected nodes
            for cj in childs:
                v_update = alpha * v / max(1, l_out)**beta

                if cj in dic_d:
                    dic_d[cj] += v_update
                else:
                    dic_d[cj] = v_update


                # Update a dictionnary to avoid duplicate evolution
                if cj in S_tosee:
                    S_tosee[cj] += v_update
                else:
                    S_tosee[cj] =  v_update

        # Update what need to be seen
        S_see = S_tosee

    return dic_d


def matrix_PageRank(S, dic, kmax=3, alpha=1, beta=1, tol=0.00001, debug=False):
    """
    Compute distance matrix based on number of hop, and min distance.
    Symmetrize the matrix, but avoid looping:
    
        A -> B0 -> C
        A -> B1 -> C
        
    Distance between B0 and B1 is infinity

    :param S: set of item (subset of the dic keys) for which you want distance
    :param dic: normal direction 
    :param dici: reverse direction 
    :param kmax: maximal number of hop (4 is good, after too long)
    :param alpha: decay factor over distance 
    :param beta: mixing factor in/out
    :rparam: (dic of distance, names)
    """

    dic_c_i = dict([(c, idx) for idx, c in enumerate(S)])

    # Iter over all items to estimate contribution for all
    dic0 = {}
    for ci in S:
        dic0[ci] = affinity_PageRank(ci, dic, alpha=alpha, beta=beta, kmax=kmax, tol=tol)              

    if debug:
        return dic0, dic_c_i

    print("Compute jaccard sim")
    #mat = jaccard_dict(dic0)
    mat = jaccard_similarity_reverse(dic0)

    return mat, dic_c_i

def similarity_to_distance(S, f=0.0001):
    """
    Easy way to transform similarity matrix [0, 1] to distance matrix [0, infty]
    
    :param S: matrix
    :param f: null hypothesis, to avoid division / 0 for items with no similarity
    """
    return (1/(S+f) - 1).clip(0)
