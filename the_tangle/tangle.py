import math
import random
import queue
import numpy as np


def real_weight_recursive(node):
    if len(node.childs) == 0:
        node.new_weight = node.weight
        return node.new_weight
    else:
        sum = 0
        for child in node.childs:
            sum += real_weight_recursive(child)/2
        node.new_weight = node.weight + sum
        return node.new_weight




def mcmc_tsa(node, alpha, count=False):
    """
    Select a node using MCMC
    Recursive function.


    :param node: Node
    :param alpha: Weight between [0, +infty)
    :rparam: Node
    """


    if count:
        node.count += 1

    if len(node.childs) == 0:
        return node

    else:
        childs = []
        weights = []
        for child in node.childs:
            childs.append(child)
            weights.append(np.exp(-alpha * (node.cumulated_weight - child.cumulated_weight)))

        return mcmc_tsa(random.choices(childs, weights=weights)[0], alpha, count)



def real_weight_tsa(node, count=False):
    """
    Select a node using MCMC
    Recursive function.


    :param node: Node
    :param alpha: Weight between [0, +infty)
    :rparam: Node
    """

    if count:
        node.count += 1

    if len(node.childs) == 0:
        return node

    else:
        childs = []
        weights = []
        for child in node.childs:

            childs.append(child)
            weights.append(child.new_weight)

        return real_weight_tsa(random.choices(childs, weights=weights)[0], count)


def uniform_tsa(tangle):
    """
    Would select a node at random from the tips set.

    :rparam: Parents of the new node to create
    :rtype: list of Nodes
    """
    parents = None
    if len(tangle.tips) > 1:
        # Select at random two nodes for validation
        parents = random.sample(tangle.tips, 2)

    else:
        # Select the root node
        parents = [tangle.genesis, tangle.genesis]

    return parents


def update_confirming_parents(node, confirming, tangle):
    if node.id not in tangle.update_confirming_tmp:
        tangle.update_confirming_tmp[node.id] = True
        node.confirming.add(confirming)
        node.cumulated_weight = len(node.confirming)

        for parent in node.parents:
            update_confirming_parents(parent, confirming, tangle)


class Node:
    def __init__(self, ID, parents, weight):
        self.id = ID # Ne pas mettre "id", c'est réservé pour python
        self.parents = parents
        self.childs = []
        self.tips_set = set()
        self.tips_set_computed = False
        self.weight = weight
        self.cumulated_weight = 1
        self.count = 0
        self.new_weight = 1
        self.confirming = set()
        self.proba_new = 0
        self.proba_mcmc = 0
        return

    def add_child(self, child):
        self.childs.append(child)
        return

    def get_tips_set(self):
        """
        Recursive function for computing.

        """

        if self.tips_set_computed:
            # Ending condition
            return self.tips_set

        if len(self.childs) == 0:
            self.tips_set.add(self.id) # Add only ID of the terminal nodes
            self.tips_set_computed = True
            return self.tips_set

        else:
            for child in self.childs:
                self.tips_set = self.tips_set | child.get_tips_set()
            self.tips_set_computed = True
            return self.tips_set



class Tangle:
    def __init__(self, h, l, tsa):
        """

        :param h: latency ?
        :param l: Generation rate, time increment by sim exp(-1/rate)
        """
        self.genesis = Node(0, [], 1)
        self.genesis.confirming = set([0])
        self.genesis.new_weight = 1
        self.count = 1
        self.time = 0
        self.latency = h
        self.rate = l
        self.tips = set([self.genesis])
        self.txs = [self.genesis]
        self.wait = queue.Queue()
        self.tsa = tsa # (method, weight)

        # self.tips_count = []
        return

    def new_node(self):
        """
        Generate a node and put it in the queue
        """

        if self.tsa[0] == "uniform":
            parents = uniform_tsa(self) # Do not use the weight

        elif self.tsa[0] == "mcmc":
            # l'iteration permet de généraliser plus facilement à k validations.
            parents = [mcmc_tsa(self.genesis, self.tsa[1]) for _ in range(2)]

        elif self.tsa[0] == "real_weight":
            parents = [real_weight_tsa(self.genesis) for _ in range(2)]

        else:
            print("Not implemented for `{}`".format(self.tsa))
            assert(False)

        # Ça sert à rien ...
        #self.tips_count.append(len(self.tips))

        # Increment node count
        self.count += 1

        # Feed the queue
        # Current time + latency for validation
        self.wait.put([self.time + self.latency, Node(self.count - 1, parents, 1)])


    def add_node(self, node):
        for parent in node.parents:
            parent.childs.append(node)

        self.update_confirming_tmp = {}

        if self.tsa[0] != "uniform":
            update_confirming_parents(node, node.id, self)

        for parent_i in node.parents:
            if parent_i in self.tips:
                self.tips.remove(parent_i)


        self.tips.add(node)
        self.txs.append(node)


    def compute_real_weight(self):
        real_weight_recursive(self.genesis)

    def run(self, n):
        """
        Add nodes to the tangle

        :param n: number of nodes to add
        :rparam: None
        """
        for i in range(n):
            # Create a new node and put it in the queue
            self.new_node()
            self.compute_real_weight()
            # Time between transactions
            self.time += np.random.exponential(1/self.rate)

            # Tant que le temps actuel pour le 1er noeud dans la queue est inférieur au temps normal
            while(self.wait.queue[0][0] < self.time):
                # Gather the first item
                # Oldly "poped"
                time_x, my_node = self.wait.get()

                self.add_node(my_node)
                '''
                # Add reciprocity relationship

                my_node.parents[0].childs.add(my_node)
                my_node.parents[1].childs.add(my_node)

                self.update_confirming_tmp = {}

                if self.tsa[0] != "uniform":
                    update_confirming_parents(my_node, my_node.id, self)

                for parent_i in my_node.parents:
                    if parent_i in self.tips:
                        self.tips.remove(parent_i)


                self.tips.add(my_node)
                self.txs.append(my_node)
                '''

                if len(self.wait.queue) == 0:
                    break


        if self.tsa[0] == "uniform":
            for tx in self.txs:
                tx.cumulated_weight = 0

        return


    def compress(self,  debug=False):
        """
        Transform initial graph on a smaller graph

        :param debug:
            - True: return edge + vertices set
            - False: return dico + correspondance dict (initial_key -> new_key)
        :rparam: tuple
        """
        print("\tComputing tips sets")

        # Compression
        self.genesis.get_tips_set()


        new_vertices = set()
        new_edges    = set()

        print("\tCreating the compressed Tangle")

        for tx in self.txs:
            tx.tips_set = sorted(tx.tips_set).__str__()
            new_vertices.add(tx.tips_set)

        for tx in self.txs:
            for parent in tx.parents:
                if parent.tips_set != tx.tips_set:
                    new_edges.add((parent.tips_set, tx.tips_set))

        if debug:
            return new_vertices, new_edges

        dico = {}
        dic_idx = {}

        for idx, x in enumerate(new_vertices):
            dic_idx[x] = idx
            dico[idx] = []

        for x0, x1 in new_edges:
            dico[dic_idx[x0]].append(dic_idx[x1])

        return dico, dic_idx



    def draw(self, probas=False):
        """
        Transform the graph with the adapted format.
        Based on `self.txs` built.

        :rparam: {key: [keys]}
        """

        if probas:
            fx_name = lambda x: "{}\t{}\t{}\n{}\n{}".format(x.id, x.cumulated_weight, x.new_weight, x.proba_mcmc, x.proba_new)
        else:
                        fx_name = lambda x: "{}.{}".format(x.id, x.new_weight, x.new_weight)


        # This do the same than before, don't worry
        return dict([(fx_name(x), list(map(lambda p: fx_name(p), x.parents))) for x in self.txs])
