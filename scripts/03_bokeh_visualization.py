"""
Read .json file obtained from 02_xx.py to create html view for easier exploration.


"""
import argparse
import json
import numpy as np
import os

from bokeh.plotting import ColumnDataSource, figure, output_file, show
from bokeh.models import Segment, HoverTool, Slider, CustomJS, LinearColorMapper, RadioGroup
from bokeh.palettes import Plasma256, inferno

from bokeh.layouts import column, row
from bokeh.models.widgets import DataTable, TableColumn




def get_Ylim(Y, alpha=0.05):
    """
    Allows to get x and y axis with same scaling
    
    :param ax: figure axis
    :rparam: None
    """
    a0, b0 = Y.min(axis=0)
    a1, b1 = Y.max(axis=0)
    
    d0 = a1 - a0
    d1 = b1 - b0
    
    if d0 > d1:
        dd = (d0 - d1)/2
        dddd = alpha * d1
        
        return a0 - dddd, a1 + dddd, b0-dd - dddd, b1+dd + dddd
    else:
        dd = (d1 - d0)/2
        dddd = alpha * d1
        return a0-dd - dddd, a1+dd + dddd, b0 - dddd, b1 + dddd  



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Tangle visualization.')

    parser.add_argument('file', type=str, 
            help='json file to load.')

    parser.add_argument('--out', default="../exports/html/", type=str,
                        help='Folder to export')
    
    parser.add_argument('--size', default=1000, type=int,
                        help='Plot size. Default: 1000')

    parser.add_argument('--color', default="Tips", type=str,
                        help='Color scheme.')
    parser.add_argument('--show_axis', default=False, action="store_true",
                        help='Show axis or not.')

    args = parser.parse_args()

    if os.path.exists(args.file) == False:
        assert(False)
    
    if os.path.exists(args.out) == False:
        os.mkdir(args.out)
        print("Created the storage folder")

    path_save = args.out + args.file.rsplit("/", 1)[1].rsplit(".", 1)[0] + ".html"
    data = {}
    with open(args.file, "r") as fp:
        data = json.load(fp)

        

    assert("Y" in data)
    if args.color not in data:
        print("Color key not in the loaded file")
        print(data.keys())
        assert(False)

    Y = np.array(data["Y"]).T

    dico = {"x": Y[:, 0], "y": Y[:, 1]}
    TOOLTIPS = []
    
    n = len(Y)
    color_keys = []
    for keys, vals in data.items():
        if keys in ["Y", "metadata", "Links"]:
            continue
           
        if len(vals) != n:
            continue
            
        TOOLTIPS.append((keys, "@" + keys))
        
        if isinstance(vals[0], list):
            dico[keys] = list(map(lambda x: ",".join(list(map(str, x))), vals))
        else:    
            dico[keys] = vals
            color_keys.append(keys) # Assume float ...
            
    print("Found {} colors scheme".format(len(color_keys)))
    print(color_keys)

    s = 15
    dico["sz0"] = np.ones(len(Y)) 
    dico["sz"] = np.ones(len(Y)) * s
    dico["colors"] = np.array(dico[args.color]).astype(float)
    dico["colors"] = dico["colors"] / dico["colors"].max()
    
        
    source = ColumnDataSource(data=dico)
        
    a, b, c, d = get_Ylim(Y)
    p = figure(plot_width=args.size, plot_height=args.size, x_range=(a,b), y_range=(c,d),
               tools="pan,lasso_select,wheel_zoom,box_select,tap,reset,save",
               title="")


    # Remove ticks and grids
    p.xgrid.visible = False
    p.ygrid.visible = False
    if args.show_axis == False:
    
        p.xaxis.major_label_text_font_size = '0pt'  # turn off x-axis tick labels
        p.yaxis.major_label_text_font_size = '0pt'  # turn off y-axis tick labels

        p.xaxis.major_tick_line_color = None  # turn off x-axis major ticks
        p.xaxis.minor_tick_line_color = None  # turn off x-axis minor ticks

        p.yaxis.major_tick_line_color = None  # turn off y-axis major ticks
        p.yaxis.minor_tick_line_color = None  # turn off y-axis minor ticks



    #############
    # Add links #
    #############

    glyph = None
    if ("Links" in data):
        pairs = []
        for x, lst in data["Links"]:
            pairs.extend(list(map(lambda y: [x, y], lst)))

        if len(pairs) != 0:
            pairs = np.array(pairs)

            x = np.array(data['Y'][0])
            y = np.array(data['Y'][1])

            source1 = ColumnDataSource(dict(
                    x=x[pairs[:, 0]],
                    y=y[pairs[:, 0]],
                    xm01=x[pairs[:, 1]],
                    ym01=y[pairs[:, 1]],
                )
            )
            #f4a582
            glyph = Segment(x0="x", y0="y", x1="xm01", y1="ym01", line_color="grey", line_width=3, line_alpha=0.4)

        p.add_glyph(source1, glyph)
    
    # Coloring according to a 0-1 value
    mapper = LinearColorMapper( palette=Plasma256, low=0, high=1)
    p.circle('x', 'y', name="circ", source=source, 
             color={ "field": "colors", "transform": mapper},
             size="sz", fill_alpha=0.2)


    # Hover only for circles, not for links
    hover = HoverTool(names=["circ"], tooltips=TOOLTIPS)
    p.add_tools(hover)

    ##########
    # Slider #
    ##########

    my_slider = Slider(start=1, end=50, value=s, step=1, title="Circle size")
    code_sld_size = """
                var mult = this.value;
                var d = source_doc.data;

                for (var i=0; i < d.sz.length; i++) {
                    d.sz[i] = mult * d.sz0[i];
                }

                source_doc.change.emit();
        """

    my_slider.js_on_change("value", CustomJS(args=dict(source_doc=source),
                                                      code=code_sld_size))


    layout = row(p, my_slider)

        
    output_file(path_save, title="Tangle")
    show(layout)

    

