from the_tangle import tangle, drawing

import time
import math
import sys
import argparse

import matplotlib.pyplot as plt

sys.setrecursionlimit(10000)



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='How to test the tangle.')

    parser.add_argument("n", type=int, default=100,
                       help="Number of points to generate")
    parser.add_argument("h", type=int, default=1,
                       help="parameter h")

    parser.add_argument("l", type=int, default=10,
                       help="parameter l, latency")

    # Si tu as une valeur par defaut, mettre --/- sinon, default pas pris en compte
    parser.add_argument("--tsa",
            choices=["uniform", "mcmc", "real_weight"], # Choices t'empeches de faire des choix hazardeux
            type=str,
            default="uniform",
            help="parameter TSA, tip selection algorithm")


    parser.add_argument("--alpha",
            type=float,
            default=0.01,
            help="parameter alpha : value alpha for MCMC")

    parser.add_argument("--feather",
            type=int,
            default=50,
            help="number of sites in the feather attack")

    parser.add_argument('--export', type=str, default="../exports/test",
                        help='Where to save the files')

    args = parser.parse_args()


    path_out = args.export
    path_u = path_out + "_raw_{}.png".format(args.tsa)
    path_c = path_out + "_cmp_{}.png".format(args.tsa)

    n = args.n
    h = args.h
    l = args.l
    tsa = [args.tsa, args.alpha]
    m = args.feather

    alpha = args.alpha


    t = tangle.Tangle(h, l, tsa)


    t1 = time.time()

    print("Running")
    t.run(n)

    t2 = time.time()

    print("Running time : " + str(t2-t1) + "s")



    ''' FEATHER ATTACK HERE '''

    print("Simulating feather attack")


    '''
    attacked_node = t.txs[m]
    first_node = tangle.Node("F0", [], 1)
    node = first_node

    t.add_node(node)

    for i in range(1,m):
        node = tangle.Node("F"+str(i), [node], 1)
        t.add_node(node)

    tangle.new_weight_recursive(first_node)
    '''

    for tx in t.txs:
        wf_mcmc = math.exp(1-alpha*tx.cumulated_weight) * (math.exp(alpha*m)-1)/(math.exp(alpha)-1)
        wf_new = 2*(m-1-1/(2**(m-1)))

        wl_mcmc = 0
        for c in tx.childs:
            wl_mcmc += math.exp(-alpha*(tx.cumulated_weight-c.cumulated_weight))

        wl_new = 0
        for c in tx.childs:
            wl_new += c.new_weight

        tx.proba_mcmc = round(wf_mcmc/(wl_mcmc + wf_mcmc), 3)
        tx.proba_new = round(wf_new/(wl_new + wf_new), 3)

    x = []
    y = []
    z = []

    y_dic = {}
    z_dic = {}


    for tx in t.txs:
        x.append(tx.id)
        y.append(tx.cumulated_weight)
        z.append(tx.new_weight)
        y_dic[tx.id] = tx.cumulated_weight
        z_dic[tx.id] = tx.new_weight


    print("Counting the amount of random walks passing by nodes : ")


    for i in range(1000):
        tangle.mcmc_tsa(t.genesis, args.alpha, count=True)


    dico_counts = {}
    for tx in t.txs:
        dico_counts[tx.id] = tx.count


    print("Drawing")
    # Recover the json-formated graph
    dico = t.draw(probas=True)
    drawing.draw_networkx(dico, path_u)


    for tx in t.txs:
        print(tx.new_weight)
