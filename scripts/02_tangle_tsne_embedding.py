import argparse
import json
import os
import sys


import numpy as np
import matplotlib.pylab as plt
from sklearn.manifold import TSNE

from the_tangle import tangle, drawing
from the_tangle import graphs as gan



sys.setrecursionlimit(10000)


def format_links(dic_idx, dic):
    '''Transform ID into index format
    return list [idx, [id1, id2, ...]]

    :param dic_idx: {Keys: index}
    :param dic: relationship {K0: [K01, K02, ...]}
    :rparam: list
    '''
    links = []
    S = set(dic_idx)
    for ci, idx in dic_idx.items():
        links.append([idx, [dic_idx[cj] for cj in set(dic[ci]) & S]])

    return links

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='How to test the tangle.')

    parser.add_argument("n", type=int,
                       help="Number of points to generate")
    parser.add_argument("h", type=int,
                       help="parameter h, default=1")

    parser.add_argument("l", type=int,
                       help="parameter l, latency, default=10")

    parser.add_argument("--tsa",
            choices=["uniform", "mcmc"], # Choices t'empeches de faire des choix hazardeux
            type=str,
            default="uniform",
            help="parameter TSA, tip selection algorithm")


    parser.add_argument("--alpha",
            type=float,
            default=0.,
            help="parameter alpha : value alpha for MCMC")

    parser.add_argument("--line",
            default=False,
            action="store_true",
            help="Initialization over a line based on depth.")

    parser.add_argument("--no_title",
                default=False,
                action="store_true",
                help="Remove title from figure for lisibility.")

    parser.add_argument('--export', type=str, default="../exports/colors/",
                        help='Where to save the files')

    parser.add_argument('--svg', action="store_true", default=False,
                        help='Save with .svg format for better quality')
    parser.add_argument('--beta', default=0.1, type=float,
                        help='Distancing factor for initial positioning.')
    parser.add_argument('--json', action="store_true", default=False,
                        help='Save the file into .json for bokeh visualization.')

    args = parser.parse_args()


    tsa = [args.tsa, args.alpha]




    # Palette for coloring
    # More info at:
    # https://matplotlib.org/stable/tutorials/colors/colormaps.html
    Inferno256 = plt.get_cmap("inferno").colors


    param_n = args.n
    param_h = args.h
    param_l = args.l

    ###############
    # Save folder #
    ###############
    path_out = args.export

    if os.path.exists(path_out) == False:
        # Be carefull when selecting export path
        os.makedirs(path_out)
        print("Created directory at `{}`".format(path_out))


    # Count the number of file for correct indexing next
    lst = list(filter(lambda x: x.startswith("tangle_color"), os.listdir(path_out)))
    lst = set(map(lambda x: x.split("_h_")[0], lst))
    c_ID = len(lst)
    print("ID for the new file: \t{}".format(c_ID))

    base_name = path_out + "tangle_color_{}_h_{}_l_{}_n_{}_{}_{}".format(c_ID, param_h, param_l, param_n, args.tsa, args.alpha)
    print("Base:", base_name)


    ####################
    # Build the tangle #
    ####################

    TG = tangle.Tangle(param_h, param_l, tsa)

    print("Running")
    TG.run(param_n)
    print("Compress")
    # Run compression to get the tips set calculated for all nodes
    _, _ = TG.compress()
    print("Drawing")
    # Recover the json-formated graph
    dic = TG.draw()
    dic_r = gan.reverse_dict(dic)

    # Get distance from root
    depth_max = gan.depth_max(set(dic), dic_r, dic)
    depth_min = gan.depth_min(set(dic), dic_r, dic)

    # PageRank diffusion
    # kmax: exploration distance. Useless to increase it.
    # Minimal value: 3
    # Beta: [0, 1]. keep 1
    # S: similarity matrix
    # dic_idx: {node_ID: position in the matrix} (useless as nodes created in the correct order)
    print("Similarity computation (may take time)")

    S, dic_idx = gan.matrix_PageRank(set(dic), dic, kmax=30, beta=1, alpha=0.9)
    D = gan.similarity_to_distance(S)

    # TSNE embedding
    print("TSNE embedding (may take CPUs + time)")

    #Y0 = np.array([list(map(lambda x: int(x.split(".")[0]), dic_idx)),
    #    50 * np.random.randn(len(dic_idx))]).T * args.beta

    Y = None
    if args.line:
        # Allow to obtain items initialized almost as a line.
        Y = np.array([[depth_max[x] for x in dic_idx],
            1 * np.random.randn(len(dic_idx))]).T * args.beta

    perp_list = [240, 120, 60, 30]
    #perp_list = [30]
    for perp in perp_list:
        print(perp)
        if Y is None:
            Y = TSNE(perplexity=perp, metric="precomputed", square_distances=True).fit_transform(D)
        else:
            Y = TSNE(init=Y,
                perplexity=perp, metric="precomputed", square_distances=True).fit_transform(D)

    lst_colors = []
    lst_colors.append(["NodeID",   list(map(lambda x: int(x.split(".")[0]), dic_idx))])



    for name, colors in lst_colors:
        print("\t- Drawing {}".format(name))
        mx = max(colors)
        #colors = [Inferno256[int(255 * x / mx)] for x in colors]

        fig, ax = plt.subplots(figsize=(10, 10))
        for i, lst in dic.items():
            yi = Y[dic_idx[i]]
            for j in lst:
                yj = Y[dic_idx[j]]
                plt.plot([yi[0], yj[0]], [yi[1], yj[1]], "black", alpha=0.1)

        ppp = ax.scatter(Y[:, 0], Y[:, 1], alpha=0.5, c=colors,cmap="inferno");

        plt.colorbar(ppp);
        if args.no_title == False:
            ax.set_title("n={}, h={}, l={}, alpha={}".format(param_n, param_h, param_l, args.alpha));

        drawing.set_xlimylim(ax)

        # Saving
        fig.savefig(base_name + '_{}.png'.format(name))

        if args.svg: # SVG takes more space than .png
            fig.savefig(base_name + '_{}.svg'.format(name))

    def to_python_float(row):
        return list(map(lambda x: round(float(x), 3), row))

    if args.json:
        dic_export = {
                "Y": [to_python_float(Y[:, 0]), to_python_float(Y[:, 1])],
                "ID":  list(map(lambda x: int(x.split(".")[0]), dic_idx)),
                "Tips":  list(map(lambda x: len(TG.txs[int(x.split(".")[0])].tips_set), dic_idx)),
                "Weights":  list(map(lambda x: float(x.split(".")[1]), dic_idx)),
                "Links": format_links(dic_idx, dic),
                "metadata": {
                        "tsa": args.tsa,
                        "alpha": args.alpha,
                        "beta": args.beta,
                        "n": args.n,
                        "l": args.l,
                        "h": args.h,
                        }
                }

        with open(base_name + ".json", "w") as fp:
            json.dump(dic_export, fp)
