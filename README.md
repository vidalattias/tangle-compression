# Tangle Compression


# Install

Requirements:

- `python3`
- `pip3`

(replace `python3` by `python` if no conflict with `python2`)

Commands to make everything run correctly:

`pip3 install -r requirements.txt`

`mkdir exports`

`python3 setup.py install --user`

## Tests

`cd scripts/`

`python3 main.py 100 1 10`



### To use bokeh

1. First, export a `.json` file using 2nd script (turn `--json` option on)

```bash
 python3 02_tangle_tsne_embedding.py 2500 1 10 --tsa=mcmc --alpha=0.2 --beta=0.1 --json
```

2. Use bokeh scritps:

```bash
python3 03_bokeh_visualization.py ../exports/colors/tangle_color_xxx.json
```



# TODO


